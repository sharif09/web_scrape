import csv


def create_csv():
    with open("index.csv", "a") as file:
        writer = csv.writer(file)
        writer.writerow(['name', 'email'])
        file.close()


if __name__ == "__main__":
    create_csv()
