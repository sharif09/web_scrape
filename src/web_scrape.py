from bs4 import BeautifulSoup
import urllib.request
import requests
import os, errno
import csv


def main_function():
    """ scrap item info from https://www.byghjemme.dk """

    site_url = "https://www.byghjemme.dk"
    html = requests.get(site_url)
    if str(html.status_code).startswith(('2', '3')):
        root_soup = BeautifulSoup(html.content, "html.parser")
        a = root_soup.find(class_="TopProductMenu hidden-xs")

        for link_class in a.find_all(class_="clListLink"):
            for tag in link_class.findAll(lambda tag: (tag.name == 'a'), href=True):
                brand_full_url = site_url + tag['href']
                brand_html = requests.get(brand_full_url)
                if str(brand_html.status_code).startswith(('2', '3')):
                    brand_soup = BeautifulSoup(brand_html.content, "html.parser")
                    single_product_root_class = brand_soup.find(id="ctl00_plhContent_ctl02_pnlJsonList")
                    if single_product_root_class:
                        for item_class in single_product_root_class.find_all(class_="loadingProducts"):
                            item_tag = item_class.find("a")
                            for item_tag in item_class.findAll(lambda item_tag: (item_tag.name == 'a'), href=True):
                        print(single_product_root_class.find_all(class_="loadingProducts"))
                else:
                    print("URL Response Error")
    else:
        print("URL Response Error")


    # Image save directory
    # directory = 'images'
    #
    # # Create csv file
    # with open("index.csv", "w") as csv_file:
    #     writer = csv.writer(csv_file)
    #     writer.writerow(['Name', 'Price', 'Description (Beskrivelse)', 'Product Image Name', 'SKU (Varenr.)', 'Catalog number', 'Category/Breadcrumb'])
    #
    #     url = "https://www.byghjemme.dk/pi/Bathlife-M%C3%B8belpakke-Utrymme-900-Mat-sort_2308215_117140.aspx"
    #     url1 = "https://www.byghjemme.dk/pi/JACKON-Jackopor-80-polystyrenstrimler-20-mm-10x120-cm-pk-a-150-stk_2286333_118033.aspx"
    #     url2 = "https://www.byghjemme.dk/pi/Duravit-Starck-3-cisterne-til-gulvst%C3%A5ende-toilet-Back-to-wall_1724927_117380.aspx"
    #     url3 = "https://www.byghjemme.dk/pi/Oras-Electra-h%C3%A5ndvaskbatteri-6V-v%C3%A6gmonteret_2302187_117024.aspx"
    #     url4 = "https://www.byghjemme.dk/pi/Svedbergs-Smart-h%C3%A5ndvaskarmatur-Krom_2294481_117041.aspx"
    #     url5 = "https://www.byghjemme.dk/pi/Varmv%C3%A6g-gips-med-30-mm-hvid-isolering-45-x-600-x-2500-mm-pl-a-1-5-m2_2286239_118034.aspx"
    #     url6 = "https://www.byghjemme.dk/pi/Plus-Siesta-stol-i-drivt%C3%B8mmer-80-cm_3207849_162964.aspx"
    #     url7 = "https://www.byghjemme.dk/pi/Polyfilla-Rutex-filler-til-savsmuldstapet-330-g_2835907_173023.aspx"
    #     url_list = [url, url1, url2, url3, url4, url5, url6, url7]
    #     for single_url in url_list:
    #         breadcumb = ""
    #         html = requests.get(single_url)
    #         soup = BeautifulSoup(html.content, "html.parser")
    #         # root = soup.find("ul", {"class": "TopProductMenu hidden-xs"})
    #         item_image_prefix = 'https://www.byghjemme.dk'
    #         sc = soup.find(id="ShopContent")
    #         pd = sc.find(id="pDetailOuter")
    #
    #         product_name = pd.find(class_="pInfoProductName hidden-xs")
    #
    #         product_price = pd.find(class_="ProductPriceValue")
    #
    #         product_description_tab = pd.find(id="pInfoLongDesc")
    #         product_description_section = product_description_tab.find(id="etLongDesc")
    #         product_description_with_technicalInfo = product_description_section.find("article")
    #         product_description_with_technicalInfo_list = product_description_with_technicalInfo.find_all("div")
    #         # print(product_description_with_technicalInfo_list[0])  # product description without customFieldDestination
    #
    #         item_image_class = pd.find(class_="ucImageControltrImg")
    #         item_image_link = item_image_class.find(itemprop="image")
    #         item_image_url = item_image_prefix + item_image_link["src"]
    #
    #         item_no = pd.find(class_="ExtProductIDValue pull-left")
    #
    #         catalog_id = pd.find(class_="AltExtProductIDLabel pull-left")
    #
    #         breadcumb_root = soup.find(class_="breadcrumbbdy esebdy")
    #         for link in breadcumb_root.find_all("a"):
    #             breadcumb = breadcumb+"/"+str(link.text)
    #
    #         writer.writerow([product_name.text, product_price.text, product_description_with_technicalInfo_list[0].text,
    #                          str(item_image_link["src"]).split('/')[-1], item_no.text,
    #                          str(catalog_id.text).split(':')[1], breadcumb])
    #
    #         if not os.path.exists(directory):
    #             try:
    #                 os.makedirs(directory)
    #             except OSError as e:
    #                 if e.errno != errno.EEXIST:
    #                     raise
    #         item_image_url_list = item_image_url.split('/')
    #         urllib.request.urlretrieve(item_image_url, directory + '/' + item_image_url_list[-1])


if __name__ == "__main__":
    main_function()
